#URL prueba: http://localhost:8000/hi/?numbers=10,23,41,8
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
#Utlities
from datetime import datetime
import json
def hello_world(request):
    now = datetime.now().strftime('%b %dth, %Y - %H:%M hrs')

    return HttpResponse('Hello, world! {now}'.format(now=str(now)))

# def hi(request):
#     numbers = map(lambda x : int(x),request.GET["numbers"].split(","))
#     return JsonResponse({ "numbers" : sorted(numbers)},json_dumps_params={'indent': 4})

def sort_integers(request):
    ''' Return JSON response with sorted integers'''
    #import pdb; pdb.set_trace() #un debugger en tiempo de ejecución
    numbers = request.GET['numbers']
    sorted_numbers = sorted([int (i) for i in numbers.split(',')])
    data = {
        'status': 'ok',
        'numbers': sorted_numbers,
        'msj': 'Integers sorted successfully'
    }
    return HttpResponse(json.dumps(data, indent=4), content_type="application/json")

def say_hi(request, name, age):
    ''' Return a greeting'''
    if age < 12:
        message = 'Sorry {}, you are not allowed here'.format(name)
    else:
        message = 'Hello {}!, welcome to Platzigram'.format(name)
    return HttpResponse(message)

def home(request):
    return HttpResponseRedirect(reverse('users:login'))
