from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView


#Utilities
from datetime import datetime

#Form
from posts.forms import PostForm

#Models
from posts.models import Post

posts_ej = [
    {
        'name': 'Mont Blac',
        'user': 'Yésica Cortés',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=1036',
    },
    {
        'name': 'Via Láctea',
        'user': 'C. Vander',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=903',
    },
    {
        'name': 'Nuevo auditorio',
        'user': 'Thespianartist',
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'picture': 'https://picsum.photos/200/200/?image=1076',
    }
]

posts_base = [
    {
        'title': 'Mont Blanc',
        'user': {
            'name': 'Yésica Cortés',
            'picture': 'https://picsum.photos/60/60/?image=1027'
        },
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo': 'https://picsum.photos/800/600?image=1036',
    },
    {
        'title': 'Via Láctea',
        'user': {
            'name': 'Christian Van der Henst',
            'picture': 'https://picsum.photos/60/60/?image=1005'
        },
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo': 'https://picsum.photos/800/800/?image=903',
    },
    {
        'title': 'Nuevo auditorio',
        'user': {
            'name': 'Uriel (thespianartist)',
            'picture': 'https://picsum.photos/60/60/?image=883'
        },
        'timestamp': datetime.now().strftime('%b %dth, %Y - %H:%M hrs'),
        'photo': 'https://picsum.photos/500/700/?image=1076',
    }
]
def list_posts_ej1(request):
    ''' List existing posts '''
    content = []
    for post in posts_ej:
        content.append(
            '''
            #<p><strong>{name}</strong><p>
            #<p><small>{user} - <i>{timestamp}</i></small><p>
            #<figure><img src="{picture}"/></figure>
            '''.format(**post)
        )
        #format(**post) == format(name=post['name']...)

    return HttpResponse('<br>'.join(content))

@login_required
def list_posts2(request):
    ''' List existing posts '''
    return render(request, 'posts/feed2.html', {'posts':posts_base})


@login_required
def list_posts(request):
    ''' List existing posts '''
    posts = Post.objects.all().order_by('-created')
    return render(request, 'posts/feed.html', {'posts':posts})

@login_required
def create_posts(request):
    ''' Create new posts '''
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('feed')
    else:
        form = PostForm()
    return render(
            request=request,
            template_name='posts/new.html',
            context={
                'profile':request.user.profile,
                'user':request.user,
                'form':form
            }
        )


class PostsFeedView(LoginRequiredMixin, ListView):
    """Return all published posts."""

    template_name = 'posts/feed.html'
    model = Post
    ordering = ('-created',)
    paginate_by = 2
    context_object_name = 'posts'