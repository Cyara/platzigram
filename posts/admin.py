from django.contrib import admin
from posts.models import Post
# Register your models here.

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('pk', 'title', )
    list_display_links = ('title',)

    search_fields = ('title', 'user__username', 'user__email')
    readonly_fields = ('created', 'updated',)