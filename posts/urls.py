"""Platzigram URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.generic import TemplateView

# Posts urls
from posts import views as posts_views
# API Posts urls
from posts import viewsApi

#API REST
from rest_framework import routers
router = routers.DefaultRouter()


#API
# En el router vamos añadiendo los endpoints a los viewsets
router.register('list_post', viewsApi.PostViewSet)


urlpatterns = [
    #Posts
    # path(
    #     route='',
    #     view=posts_views.list_posts,
    #     name="feed"
    # ),
    path(
        route='',
        view=posts_views.PostsFeedView.as_view(),
        name='feed'
    ),
    path('demo/', posts_views.list_posts2, name="feed2"),
    path('new/', posts_views.create_posts, name="create_posts"),
    path('ej1/', posts_views.list_posts_ej1, name="post_ej"),

    path('api/', include(router.urls)),

]
