# Platzigram

Proyecto realizado en Django basado en Instagram, se hace uso de los template tag pero se construye con servicios REST con Django REST Framework para posteriormente separar el frontend del backend e incorporar cualquiér otro tipo de framewoks como Angular, React, Svelte o Vue.

Proyecto completamente funcional para la gestión de usuarios, posts, servicios REST entre otros.

### Ejecutar proyecto

```
$ python manage.py makemigrations
```

```
$ python manage.py migrate
```

```
$ python manage.py runserver
```

### Crear usuarios o superususarios

```
$ python manage.py createsuperuser
```
```
$ python manage.py createuser
```



# Lista de rutas

| Sección  |  Ruta |
| ------------ | ------------ |
| API |  /users/api/list_profiles/ |
| API | /posts/api/list_post/ |
| Users | login/ |
| Users | logout/ |
| Users | signup/view/ |
| Users | signup/ |
| Users | me/profile/ |
| Users | <str:username>/ |
| Users | Middlewares | me/profile |
| Posts | feed |
| Posts | demo/ |
| Posts | new/ |
| Posts | ej1/ |
| Platzigram | admin/ |


# Usuaros de prueba

| usuario  |  contraseña |
| ------------ | ------------ |
| admin | admin |
| Cristopher | admin_9876 |
| Gepeto | admin_9876 |
| mauro | admin_9876 |

# Información

| Nombre  |  Email |
| ------------ | ------------ |
|  Cristhian Yara |  cmyarap@gmail.com |